require("lazy").setup({
    {'nvim-lua/plenary.nvim'},
    {'sharkdp/fd'},
    {'jamessan/vim-gnupg'},
    {url="https://gitlab.com/HiPhish/rainbow-delimiters.nvim.git"},
    {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.5',
        dependencies = {'nvim-lua/plenary.nvim'}
    },
    {
        "rose-pine/neovim",
        lazy = false,
        priority = 1000,
        name = 'rose-pine'
    },
    {'nvim-treesitter/nvim-treesitter', build = ':TSUpdate'},
    {'theprimeagen/harpoon'},
    {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v2.x',
        dependencies = {
            -- LSP Support
            {'neovim/nvim-lspconfig'}, -- Required
            {'williamboman/mason.nvim'}, -- Optional
            {'williamboman/mason-lspconfig.nvim'}, -- Optional
            -- Autocompletion
            {'hrsh7th/nvim-cmp'}, -- Required
            {'hrsh7th/cmp-nvim-lsp'}, -- Required
            {'L3MON4D3/LuaSnip'} -- Required
        }
    }
})
