The configuration files for Neovim 0.9.4 that work on my computer. It has all the good stuff like Telescope, Mason and lsp-zero.

Based on the video of Theprimeagen  "0 to LSP : Neovim RC From Scratch" https://www.youtube.com/watch?v=w7i4amO_zaE

You can find the original config files here: \
https://github.com/ThePrimeagen/init.lua

Updated with some small tidbits, left out stuff that I don't need and added 'vim-gnupg' plugin to manage GPG.

This will need some updates to make it perfect for my ZSA Moonlander keyboard and other stuff.
